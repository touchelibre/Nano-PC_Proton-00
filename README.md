[Site Web du Projet](http://touchelibre.fr)

# Nano PC : Proton-00

![Logo Projet](01_Système/05_Logo/Logo_ToucheLibre_V3.png)


## Présentation du Projet

![3D de l’objet](02_Mécanique/03_Plan_3D/nano-PC_+_ecran.png)

![Photo du Nano PC](05_Fabrication/02_Fabri_Méca/Photos/P1010672.JPG)

Le projet Proton-01 a pour but d’expérimenté l’utilisation d’un nano PC pour voir comment on peut l’intégré au projet ToucheLibre global. Il s’agit de traiter d’idée de rendre le clavier intelligent.

L’autre intérêt de ce Proton-01 est pédagogique. Il permet de m’exercer sur le travail du bois et l’esthétique. Et aussi de s’exercer sur l’habillage d’un écran existant avec du bois.


## Avancement

Voici le reste à faire et l’intention final du projet. L’avancement est donné entre accolade {x %}.

* [x] {100%} Spécification générale
* [x] {100%} Choix des composants
* [x] {100%} Dessin du boitier en 3D
* [x] {100%} Fabrication de la boîte nano PC
* [ ] {90%} Fabrication de la carte électronique d’interface
	* [x] Fabrication
	* [ ] Test de bon fonctionnement
- [ ] {10%} Fabrication de l’habillage de l’écran
- [ ] {0%} Test d’intégration fonctionnel


## Les dossiers

Le dossier **01_Système** contient tous les éléments architecturant, la spécification générale du produit et toutes les informations qui consernent potentiellement tous les métiers

Les dossiers **02_ à 04_** servent à la conception du produit. Il contient toute l’étude et toutes les sources. Nous sommes organisés par métier. Ici on trouve la mécanique l’électronique, le logiciel, mais on pourrait en imaginer d’autres selon la nature du projet.

Le dossier **05_Fabrication** est destiné à ceux qui veulent  simplement fabriquer le produit sans trop réfléchir au détail de la conception. Ce dossier est un peu l’équivalent de la rubrique «Téléchargement» pour un projet de type logiciel libre. Mais évidement, un objet libre ne se fabrique pas aussi facilement qu’un simple téléchargement. La philosophie de ce dossier est d’expliquer le plus simplement possible comment fabriquer l’objet.


## Licence

La licence attribuée est différente selon le métier considéré. Aussi, il y a une licence différente dans chaque dossier selon la répartition suivante :

| Dossier         | Licence (SPDX identifier) |
| --------------- | ------------------------- |
| 01_Système      | CC-BY-SA-4.0-or-later     |
| 02_Mécanique    | CERN-OHL-S-2.0-or-later   |
| 03_Électronique | CERN-OHL-S-2.0-or-later   |
| 04_Logiciel     | GNU GPL-3.0-or-later      |
| 05_Fabrication  | CC-BY-SA-4.0-or-later     |

Voir <https://spdx.org/licenses/>


## À Propos de l’Auteur

__Lilian Tribouilloy :__

* __Formation :__ Ingénieur en électronique, diplômé de l’[ENSEA](https://www.ensea.fr/fr) en 2004.
* __Métier :__ Concepteur électronique spécialisé dans les radiofréquences et la CEM (Compatibilité ÉlectroMagnétique).
* __Exemples de produit conçu dans le cadre professionnel :__ Émetteur et Réémetteur pour la télévision numérique ; Amplificateur de puissance classe AB pour une modulation OFDM ; Calculateur entrées/sorties pour camion ; Tableau de bord pour véhicule spéciaux ; Boîtier de télématique pour la gestion de flotte.
* __Objet Libre conçu :__ [ToucheLibre](http://touchelibre.fr/), un clavier d’ordinateur ergonomique en bois. Alliant l’esthétique à l’utile, il s’inscrit dans des valeurs de liberté, d’écologie et de santé.


