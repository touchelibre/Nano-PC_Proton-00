EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A2 23386 16535
encoding utf-8
Sheet 1 2
Title "Electonic Schematics"
Date "14 juin 2020"
Rev "A"
Comp "Lilian T."
Comment1 "TL 500 024"
Comment2 "Nano PC"
Comment3 "Interconnexion"
Comment4 "Tracking Sheet"
$EndDescr
$Sheet
S 15000 14500 2000 1000
U 5EE677E2
F0 "Connecteurs & Interrupteurs" 50
F1 "Connecteurs_Interrupteurs.sch" 50
$EndSheet
Wire Notes Line style solid
	2500 3000 10200 3000
Wire Notes Line style solid
	2500 3000 2500 7500
Wire Notes Line style solid
	2500 3300 10200 3300
Text Notes 5000 3200 0    79   ~ 16
Core Design Tracking
Wire Notes Line style solid
	2500 4500 10200 4500
Wire Notes Line style solid
	2900 4200 2900 7500
Text Notes 2600 4400 0    79   ~ 16
Rev.
Text Notes 5400 4400 0    79   ~ 16
Description
Wire Notes Line style solid
	9200 4200 9200 7500
Wire Notes Line style solid
	10200 3000 10200 7500
Wire Notes Line style solid
	2500 7500 10200 7500
Text Notes 9550 4400 0    79   ~ 16
Date
Text Notes 6400 3200 0    59   Italic 0
(Modification of all the variants or layout impact)
Wire Notes Line style solid
	2500 4900 10200 4900
Wire Notes Line style solid
	2500 6200 10200 6200
Text Notes 2650 4750 0    79   ~ 0
A
Text Notes 3100 4750 0    79   ~ 0
Creation of schematics. Before Layout.
Text Notes 9300 4750 0    79   ~ 0
14 juin 2020
Wire Notes Line style solid
	13000 3000 20700 3000
Wire Notes Line style solid
	13000 3000 13000 7500
Wire Notes Line style solid
	13000 3300 20700 3300
Text Notes 14200 3200 0    79   ~ 16
V0 Variant Tracking : BASE
Wire Notes Line style solid
	13000 4600 20700 4600
Wire Notes Line style solid
	13400 4300 13400 7500
Text Notes 13100 4500 0    79   ~ 16
Rev.
Text Notes 15900 4500 0    79   ~ 16
Description
Wire Notes Line style solid
	19700 4300 19700 7500
Wire Notes Line style solid
	20700 3000 20700 7500
Wire Notes Line style solid
	13000 7500 20700 7500
Text Notes 20050 4500 0    79   ~ 16
Date
Text Notes 16900 3200 0    59   Italic 0
(Modification of this variant on bill of material)
Wire Notes Line style solid
	13000 5000 20700 5000
Wire Notes Line style solid
	13000 6300 20700 6300
Text Notes 13100 4850 0    79   ~ 0
A00
Text Notes 13600 4850 0    79   ~ 0
Creation of schematics. Before Layout.
Text Notes 19800 4850 0    79   ~ 0
14 juin 2020
Wire Notes Line style solid
	2500 4200 10200 4200
Text Notes 2650 3950 0    79   ~ 0
List of the\nAlways Not\nMounted\nComponents
Wire Notes Line style solid
	3500 3300 3500 4200
Wire Notes Line style solid
	13000 4300 20700 4300
Text Notes 13150 3950 0    79   ~ 0
List of the\nComponents\nunder this\nvariant
Wire Notes Line style solid
	14000 3300 14000 4300
Wire Notes Line width 20 style solid
	500  1500 22900 1500
Text Notes 900  1200 0    315  ~ 63
Schéma Électronique du Nano PC ToucheLibre     ;     Carte Inteconnextion du Nano PC
Text Notes 3700 3800 0    79   ~ 0
ANM > \n\n(All the tagged components with CD (Core Design) are mounted for all the variants.)
$EndSCHEMATC
